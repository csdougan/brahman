/*
 *
 *
 *   ______              _
 *   | ___ \            | |
 *   | |_/ / _ __  __ _ | |__   _ __ ___    __ _  _ __
 *   | ___ \| '__|/ _` || '_ \ | '_ ` _ \  / _` || '_ \
 *   | |_/ /| |  | (_| || | | || | | | | || (_| || | | |
 *   \____/ |_|   \__,_||_| |_||_| |_| |_| \__,_||_| |_|
 *
 *
 *  "The creative principle which lies realized in the whole world"
 *
 *  Copyright (c) Strand Games 2020.
 *
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License (LGPL) as published
 *  by the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 *  for more details.
 * 
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *  contact@strandgames.com
 *
 */


#pragma once

// reserved words

#define SYM_NAME    "name"
#define SYM_LABEL   "label"
#define SYM_SET     "set"
#define SYM_PUT     "put"
#define SYM_IS      "is"

// special terms
#define TERM_PLAYER  "PLAYER"
#define TERM_THING  "THING"
#define TERM_GAME_FILES "GAME_FILES"

#define TERM_LAST   "LAST"
#define TERM_IT     "IT"
#define TERM_TICK   "TICK"

// special properties
#define PROP_IN     "in"

// match empty result
#define MATCH_NULL  "null"

// match whole set
#define MATCH_THEM  "them"

#define EXEC_SYNTAX "syntax"
#define EXEC_ALREADY "already"
#define EXEC_UNABLE  "unable"
#define EXEC_OK     "ok"
#define EXEC_TRUE   "yes"
#define EXEC_FALSE  "no"






