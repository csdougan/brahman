# Changelog

All notable changes to The Guild of Thieves game will be documented in this file.

## 1.2.0 (2020-09-04)

* Improved font for Timex hi-res mode.
* Smoother location image transitions.

## 1.1.0 (2020-08-04)

* Game story file guild.prg is trimmed from unused symbols and relocation
information to significantly reduce its loading time.

## 1.0.0 (2020-07-18)

* First public release.
