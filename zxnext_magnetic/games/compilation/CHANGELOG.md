# Changelog

All notable changes to The Magnetic Scrolls Compilation will be documented in
this file.

The changes to the individual games in the compilation are documented separately
here:

* [The Pawn](../pawn/CHANGELOG.md)
* [The Guild of Thieves](../guild/CHANGELOG.md)
* [Jinxter](../jinxter/CHANGELOG.md)
* [Corruption](../corrupt/CHANGELOG.md)
* [Fish!](../fish/CHANGELOG.md)
* [Myth](../myth/CHANGELOG.md)

## 1.0.0 (2020-09-22)

* First public release.
