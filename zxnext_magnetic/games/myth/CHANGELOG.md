# Changelog

All notable changes to the Myth game will be documented in this file.

## 1.1.0 (2020-09-04)

* Improved font for Timex hi-res mode.
* Smoother location image transitions.

## 1.0.0 (2020-07-18)

* First public release.
